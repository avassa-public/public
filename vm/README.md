# Avassa Edge Enforcer VM

**NOTE** This VM image is not for production use, only for testing.

## Installing the Edge Enforcer
There are two options for installing the Edge Enforce, either by manually providing the information, or by using Cloud Init.

### Manual Install
Boot the VM image and login as root, no password needed. If the EE has not yet been installed, an installation script is automatically launched.

Please provide credentials and a call home server as asked for by the script.

```
Welcome to Alpine Linux 3.13
Kernel 5.10.29-0-virt on an x86_64 (/dev/ttyS0)

localhost login: root

Avassa Edge Enforcer

> Avassa Edge Enforcer Installer
Username: my-username
Password:
Call home server: ct.my-company.com

> Downloading Avassa Software
Login Succeeded
registry.gitlab.com/avassa/code/supd:latest

> Configuring Edge Enforcer

> Starting Edge Enforcer
 * service supd added to runlevel default
 * Starting supd ...
 [ ok ]

> Edge Enforcer started
Your host id is ba706c81-cdba-4261-79ef-117b01de6ba8

Example host configuration
name: my-new-site
type: edge
topology:
  parent-site: topdc
labels:
  type: edge
  name: my-new-site
hosts:
  - host-id: ba706c81-cdba-4261-79ef-117b01de6ba8
```

### Cloud Init
Create a cloud init file and write the avassa config, e.g.
```
#cloud-config
write_files:
  - path: /root/.avassa.cfg
    permissions: '0600'
    content: |
      username: my-username
      password: my-password
      callhome_server: ct.my-company.com
```

The host id can be found by running `cat /sys/class/dmi/id/product_uuid`.
