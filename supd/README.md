# Install Edge Enforcer using the scripts

## Create a user with software download capabilities
In the Control Tower UI, select users, click create new user.

Give the user a name, email address (can be whatever, no email is sent) and a password. Make sure you only select the `pull-supd` policy.

![image-1.png](./image-1.png)

## Install the Edge Enforcer

### Debian
```shell
curl -OL https://gitlab.com/avassa-public/public/-/raw/master/supd/install-ee-debian11
chmod +x install-ee-debian11
```

The URL below is the Control Towers URL, in my case this is `demo-1.the-company.avassa.net`, **NOTE** make sure you remove any `https://`.

```shell
sudo ./install-ee-debian11 sw@avassa.io <secret password> demo-1.the-company.avassa.net
```

This will install the Edge Enforcer and all prerequisites.

After a short while it will finish.


## Registering the host
After installing the Edge Enforcer, run the following command to get its host-id
```shell
sudo docker exec supd /supd/bin/supd hostid
```
This will output a string, this string is the host-id and should be used when registering this host at a site.
